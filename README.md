This project is about taking image snapshots of a single event at the same time in a distributed way. We provide supports to synchronize clock among multiple device and send files among each other using a go shared library. From the application side we call go function to start the clock synchronization when it joins the network and later it can show the synchronized local time on top of the camera interface.

Before being able to run this program there are few things that need to be done:

Grab the latest copy of the Android NDK from https://developer.android.com/tools/sdk/ndk/index.html. Make it executable and run it. Unpack it in your HOME directory or wherever you want. 

Next cd into the directory where you have downloaded the Android NDK and get a copy of our platform NDK using the following command:

```
#!bash

export NDK_ROOT=~/android-ndk-toolchain/
./android-ndk-r10d/build/tools/make-standalone-toolchain.sh --platform=android-19 --arch=arm --install-dir=$NDK_ROOT

```

You might have to change the build.bash file placed inside EventCapturer/go_src folder based on where you have downloaded this project, Android NDK and go source files.

You also need to provide the SERVER_IP in EventCapturer/go_src/src/libclocksync/server/server.go and EventCapturer/go_src/src/libclocksync/client/client.go. It should be the ip of the machine where you are going to run the server.go.

Next cd into EventCapturer/go_src folder and run the build.bash file

```
#!bash

./build.bash

```

Next run the server.go from EventCapturer/go_src/src/libclocksync/server folder.

Then you can run this project from Android Studio in an emulator or a device. For device, go 1.4 has some issues with any Android release after the Jelly Bean, so it doesn't work for a device with Kitkat or Lollipop, though it works for emulator.