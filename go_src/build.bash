#!/usr/bin/env bash

set -e

export GO_DIR=/usr/local/go
export ANDROID_APP=~/AndroidStudioProjects/EventCapturer
export NDK_CC=~/android-ndk-toolchain/bin/arm-linux-androideabi-gcc
export GOPATH=$ANDROID_APP/go_src
export PATH=$PATH:$GOPATH/bin

cd $GO_DIR/src
sudo ./make.bash

cd $GOPATH/src/libclocksync
gobind -lang=go libclocksync/clocksync > clocksync/go_clocksync/go_clocksync.go
gobind -lang=java libclocksync/clocksync > $ANDROID_APP/app/src/main/java/go/clocksync/Clocksync.java 

cd $GO_DIR/src 
sudo CC_FOR_TARGET=$NDK_CC CGO_ENABLED=1 GOOS=android GOARCH=arm GOARM=7 ./make.bash 

cd $GOPATH/src/libclocksync
CGO_ENABLED=1 GOOS=android GOARCH=arm GOARM=7 \
	go build -ldflags="-shared" .
mv -f libclocksync $ANDROID_APP/app/src/main/jniLibs/armeabi-v7a/libgojni.so

cd $GO_DIR/src
sudo ./make.bash