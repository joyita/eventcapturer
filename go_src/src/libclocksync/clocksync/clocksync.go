package clocksync

import (
	"libclocksync/client"
)

func Start(_localIp string) {
	client.Setup(_localIp)
}

func GetLocalTime() int64 {
	return int64(client.GetLocalTime())
}
