package main

import (
	"fmt"
	"net"
	"os"
	"strings"
	"sync"
)

var (
	SERVER_IP         string = "192.168.1.108"
	SERVER_PORT       string = "5000"
	MASTER_IP         string
	MASTER_PORT       string
	MASTERREQ         string = "Masterreq"
	MASTERSETUP       string = "Mastersetup"
	HAVEMASTER        string = "Havemaster"
	NOMASTER          string = "Nomaster"
	MASTERSETUPACK    string = "Mastersetupack"
	BUFFERSIZE        int    = 1024
	hasMaster         bool   = false
	mutexMasterIpport sync.Mutex
)

const debug bool = true

func main() {
	ln, err := net.Listen("tcp", ":"+SERVER_PORT)
	checkError(err)
	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println("Error in accpeting connetion")
			continue
		}
		go handleClient(conn)
	}
}

func handleClient(conn net.Conn) {
	defer conn.Close()
	for {
		buf := make([]byte, BUFFERSIZE)
		n, err := conn.Read(buf[0:])
		if err != nil {
			return
		}
		var request string = string(buf[0:n])
		var response string
		tokens := strings.Split(request, ":")
		if debug {
			fmt.Println("Recieved request from", conn.RemoteAddr().String(), ":", request)
		}
		if strings.EqualFold(tokens[0], MASTERREQ) {
			mutexMasterIpport.Lock()
			if hasMaster {
				response = HAVEMASTER + ":" + MASTER_IP + ":" + MASTER_PORT
			} else {
				response = NOMASTER
			}
			mutexMasterIpport.Unlock()
			if debug {
				fmt.Println("Reply to", conn.RemoteAddr().String(), ":", response)
			}
			_, err := conn.Write([]byte(response))
			if err != nil {
				fmt.Println("Failed to write to client")
			}
			if strings.Contains(response, HAVEMASTER) {
				return
			}
		} else if strings.EqualFold(tokens[0], MASTERSETUP) {
			mutexMasterIpport.Lock()
			hasMaster = true
			MASTER_IP = tokens[1]
			MASTER_PORT = tokens[2]
			response = MASTERSETUPACK
			mutexMasterIpport.Unlock()
			if debug {
				fmt.Println("Reply to", conn.RemoteAddr().String(), ":", response)
			}
			_, err := conn.Write([]byte(response))
			if err != nil {
				fmt.Println("Failed to write to client")
			}
		} else {
			fmt.Println("Cannot understand your request, please try again")
			return
		}

	}
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s ", err.Error())
		os.Exit(1)
	}
}
