// Java Package clocksync is a proxy for talking to a Go program.
//   gobind -lang=java libclocksync/clocksync
//
// File is generated by gobind. Do not edit.
package go.clocksync;

import go.Seq;

public abstract class Clocksync {
    private Clocksync() {} // uninstantiable
    
    public static long GetLocalTime() {
        go.Seq _in = new go.Seq();
        go.Seq _out = new go.Seq();
        long _result;
        Seq.send(DESCRIPTOR, CALL_GetLocalTime, _in, _out);
        _result = _out.readInt64();
        return _result;
    }
    
    public static void Start(String _localIp) {
        go.Seq _in = new go.Seq();
        go.Seq _out = new go.Seq();
        _in.writeUTF16(_localIp);
        Seq.send(DESCRIPTOR, CALL_Start, _in, _out);
    }
    
    private static final int CALL_GetLocalTime = 1;
    private static final int CALL_Start = 2;
    private static final String DESCRIPTOR = "clocksync";
}
