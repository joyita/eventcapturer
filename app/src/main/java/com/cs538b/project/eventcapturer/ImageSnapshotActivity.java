package com.cs538b.project.eventcapturer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ImageSnapshotActivity extends ActionBarActivity {

    private Intent mIntentCameraActivity;
    private Button mButtonTakePicture;
    private List<String> listOfImagesPath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_snapshot);
        mIntentCameraActivity = new Intent(this, CameraActivity.class);

        mButtonTakePicture = (Button) findViewById(R.id.btnFullImage);
        mButtonTakePicture.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(mIntentCameraActivity);
                    }
                }
        );

        GridView gridview = (GridView) findViewById(R.id.gridView);
        listOfImagesPath = null;
        listOfImagesPath = RetriveCapturedImagePath();
        if(listOfImagesPath!=null){
            gridview.setAdapter(new ImageAdapter(this, listOfImagesPath));
        }

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

            }
        });
    }

    // Some lifecycle callbacks so that the image can survive orientation change
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private List<String> RetriveCapturedImagePath() {
        List<String> tFileList = new ArrayList<String>();
        File f = getAlbumStorageDir();
        if (f.exists()) {
            File[] files = f.listFiles();
            Arrays.sort(files);

            for(int i = 0; i < files.length; i++){
                File file = files[i];
                if(file.isDirectory())
                    continue;
                tFileList.add(file.getPath());
            }
        }
        return tFileList;
    }

    private File getAlbumStorageDir(){
        File storageDir = null;
        String CAMERA_DIR = "/dcim/";

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = new File (
                    Environment.getExternalStorageDirectory() +
                            CAMERA_DIR +
                            getAlbumName()
            );

            if (storageDir != null) {
                if (! storageDir.mkdirs()) {
                    if (! storageDir.exists()){
                        Log.d("EventCapturer", "failed to create directory");
                        return null;
                    }
                }
            }
        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    private String getAlbumName() {
        return getString(R.string.album_name);
    }
}
