package com.cs538b.project.eventcapturer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import go.Go;
import go.clocksync.Clocksync;

public class MainActivity extends Activity {

    private static boolean mLoadedFirstTime = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Go.init(getApplicationContext());
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        System.out.println("OnResume");
        if (mLoadedFirstTime == false) {
            setContentView(R.layout.activity_main_resume);
        }
    }

    public void joinNetwork(View view) {
        mLoadedFirstTime = false;
        Clocksync.Start(getLocalIpAddress());
        Intent intent = new Intent(this, ImageSnapshotActivity.class);
        startActivity(intent);
    }

    public void continueToImageSnapshotActivity(View view) {
        Intent intent = new Intent(this, ImageSnapshotActivity.class);
        startActivity(intent);
    }

    public void exitNetwork(View view) {
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}

